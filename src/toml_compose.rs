/*
    Functions for parsing toml files into full arrangements.
    Slightly messy in its current shape.
 */

/*
    [[action]]
    define = {
        808like = ["cc.c"],
        longsaw = [".[c2c].."]
    }
    [[action]]
    play: {
        lead = "longsaw",
        selected = ["808like", "longsaw"],
    }

    Problem: Multi-define here gets a bit obnoxious

    [[play]]
    {
        lead = "longsaw",
        selected = ["808like", "longsaw"],
    }

    This allows more uniform stuff which is nice.

    Next up is the available attributes.

    With a play


 */


 use std::collections::BTreeMap;
use std::collections::HashMap;
 #[allow(unused_imports)] use crate::note::Note;
 #[allow(unused_imports)] use crate::score::Score;
 #[allow(unused_imports)] use crate::tone::*;
 use serde::Deserialize;
use crate::note_strings::parse_notes;
use std::fs::read;
use std::cell::RefCell;
use std::rc::Rc;


type SynthDefs = Vec<BTreeMap<String, Vec<String>>>;
#[derive(Debug, Deserialize)]
struct DefineAction {
    synth_defs: SynthDefs
}

#[derive(Debug, Deserialize)]
struct PlayAction {
    lead: Option<String>, // lead = "longsaw",
    only: Option<Vec<String>>, // only = ["808like", "longsaw"],
    times: Option<i32>, // times = 3,
    move_cursor: Option<bool>, // move_cursor = false,
    non_repeating: Option<bool>, // non_repeating = true
    custom: Option<SynthDefs>, // custom = [{808like = ["cc.cc"]}],
    stall: Option<bool> // stall = true // "Only play this section"
}

#[derive(Debug, Deserialize)]
struct TomlCompose {
    define: Vec<DefineAction>, // Every {} under [[define]]
    play: Vec<PlayAction> // Every {} under [[play]]
}

impl PlayAction {
    fn get_used(&self, all_scores: &HashMap<String, Score>) -> HashMap<String, Score> {

        if self.only.is_some() {
            let mut filtered_map = HashMap::new();
            for filtered in self.only.as_ref().unwrap() {
                filtered_map.insert(filtered.to_string(), all_scores.get(filtered).unwrap().clone());
            }
            return filtered_map;
        }
        all_scores.clone()
    }

    fn get_with_custom(&self, all_scores: &HashMap<String,Score>) -> HashMap<String, Score> {
        let mut override_map = all_scores.clone();
        if self.custom.is_some() {

            for custom_synth_def in self.custom.as_ref().unwrap() {
                for (synth_name, note_sketches) in custom_synth_def {
                    // TODO: Not monotone hacked
                    let notes = parse_notes(&note_sketches.first().unwrap());
                    let mut new_score = Score::new();
                    new_score.read(notes);
                    override_map.insert(synth_name.to_string(), new_score);
                }
            }
        }

        override_map
    }
}

impl TomlCompose {
    // TODO:  Move in parts of parse()
}

pub type Composition = HashMap<String, Score>;


pub fn parse(toml_string: &str) -> Result<Composition, String> {

    let mut melody_map: Composition = HashMap::new();

    // TODO: Proper error handling here
    let contents: TomlCompose = toml::from_str(&toml_string).unwrap();

    /*
        1. Order will need to be handled eventually, but for now we can't
        2. Go through all define actions. Save a map of <synth,score>
            - The score() is constructed via parse->new
    */
    for define_action in &contents.define {
        for synth_def in &define_action.synth_defs {
            for (synth, note_sketches) in synth_def {

                // Ideally we should handle all given sketches in the array
                // But since we don't currently have support for polyphony in score()
                // it will have to wait
                let monophone_hack = note_sketches.first().unwrap();
                let notes = parse_notes(&monophone_hack);
                let mut melody_score = Score::new();
                melody_score.read(notes);

                // No concern for overriding at the moment
                melody_map.insert(String::from(synth), melody_score);

            }
        }
    }

    /*
        3. Go through all play actions
            - Pass define() map into a filter() method that checks if only() is used
            - Pass filteredDefine() map into override in case of custom()
            - Use filteredCustomDefine() to determine longest for default lead
                - Chose either this or unwrapped lead from action
            - For each times(default 1):
                for each filteredCustomDefine():
                    play_along(lead) OR play_then_pad(lead)
            - Unless overridden, "move the cursor", i.e. determine longest full score
                and make all others wait until the end of that one.
     */

    // Create the scores that will track the "actual" notes as they get added via "play"
    // TODO: The way things are now, this effectively breaks if you define something in custom
    //  without first adding it to regulars. That said, you can't just "add if missing"
    //  because of padding requirements, e.g. "zap" as custom in third verse will start it in first verse
    let mut full_score_map = HashMap::new();
    for (key, _) in &melody_map {
        full_score_map.insert(key.to_string(), Rc::new(RefCell::new(Score::new())));
    }

    // TODO: Ideally we should be able to "stall" on a single play action
    //  so that we for example only play play#4 over and over for jamming purposes
    //  This can be a little tricky to do a "clean" way but we can always filter the
    //  actions prior
    let mut filtered_plays = Vec::new();
    for play_action in &contents.play {
        if play_action.stall.unwrap_or(false) {
            filtered_plays.push(play_action.clone());
        }
    }

    if filtered_plays.is_empty() {
        for play_action in &contents.play {
            filtered_plays.push(play_action.clone())
        }
    } else {
        // TODO: Rather ugly to call the client here, ideally we'd have access to weather a stall
        //  happened on the top level (Maybe this should return a specialized object rather than the score map...)
        //use crate::jackdaw_client::*;
        //force_reset();
    }

    for play_action in filtered_plays {
        let filtered = &play_action.get_used(&melody_map);
        let with_custom = &play_action.get_with_custom(&filtered);

        let find_longest = |scores: &HashMap<String, Score>| -> Score {
            let mut with_longest_dur = Score::new();
            for (_, score) in scores {
                if score.duration() > with_longest_dur.duration() {
                    with_longest_dur = score.clone();
                }
            }
            with_longest_dur
        };

        let mut lead_score = find_longest(with_custom);
        if play_action.lead.is_some() {
            lead_score = with_custom.get(&play_action.lead.clone().unwrap()).unwrap().clone();
        }

        for i in 0..play_action.times.unwrap_or(1) {
            if play_action.non_repeating.is_some() && play_action.non_repeating.unwrap() {
                for (name, score) in with_custom {
                    let mut actual = full_score_map.get(name).unwrap().borrow_mut();
                    // ... not the same as "play then pad" :)
                    let play_these = score.add_and_pad(&lead_score);
                    actual.read(play_these);
                }
            } else {
                for (name, score) in with_custom {
                    // What is "play along"?
                    let mut actual = full_score_map.get(name).unwrap().borrow_mut();
                    let play_these = score.repeat_with(&lead_score);
                    actual.read(play_these);
                }
            }
        }

        let should_move_cursor = play_action.move_cursor.unwrap_or(true);

        if should_move_cursor {
            let mut longest_duration = 0.0;
            // TODO: This is a tricky place
            for score in full_score_map.values() {
                if score.borrow().duration() > longest_duration {
                    longest_duration = score.borrow().duration();
                }
            }

            // Pad everything until the end of the longest current score
            for reference in full_score_map.values() {
                let mut mut_ref = reference.borrow_mut();
                let ref_dur = mut_ref.duration();
                if ref_dur < longest_duration {
                    mut_ref.wait(longest_duration - ref_dur);
                }
            }

        }

    }

    let mut final_map = HashMap::new();
    for (k,v) in full_score_map {
        final_map.insert(k, v.borrow().clone());
    }

    Result::Ok(final_map)

}

mod test {
    use crate::toml_compose::*;

    #[test]
    fn test_basic() {
        let file_data = r#"
            [[define]]
            [[define.synth_defs]]
            brute = ["ccddgggg"]
            smol = ["ee"]

            [[play]]
            times = 2

        "#;

        let parsed = parse(file_data).unwrap();
        assert_eq!(16, parsed.get("brute").unwrap().notes().len());
        assert_eq!(16, parsed.get("smol").unwrap().notes().len());
    }

    #[test]
    fn test_longer() {
        let file_data = r#"
            [[define]]
            [[define.synth_defs]]
            brute = ["ccddgggg"]
            smol = ["ee"]
            fizz = ["f.c"]

            [[play]]
            lead = "fizz"
            only = ["fizz", "smol"]
            move_cursor = false

            [[play]]
            only = []
            move_cursor = true

            [[play]]
            times = 1

        "#;

        // The middle move_cursor part will add an additional note of silence
        let parsed = parse(file_data).unwrap();
        assert_eq!(8 + 1, parsed.get("brute").unwrap().notes().len());
        assert_eq!(3 + 8, parsed.get("smol").unwrap().notes().len());
    }

    #[test]
    fn test_custom_order() {
        let file_data = r#"
            [[define]]
            [[define.synth_defs]]
            one = ["cccc"]

            [[play]]

            [[play]]
            [[play.custom]]
            one = ["ffff"]

        "#;

        let parsed = parse(file_data).unwrap();
        let final_note = parsed.get("one").unwrap().notes().last().unwrap();
        let first_note = parsed.get("one").unwrap().notes().first().unwrap();

        // If none are quiet and both are unlike, we can expect them to have different tones as outlined
        // Direct tone check would muddy the mix with tone logic which is liable to octave changes
        assert_ne!(&0.0, final_note.amplitude());
        assert_ne!(&0.0, first_note.amplitude());
        assert_ne!(first_note.tone(), final_note.tone());
    }

    #[test]
    fn test_quick_notation() {
        let file_data = r#"
            define = [
            {synth_defs = [
                {smol = ["dd"]},
                {brute = ["ee"]}
            ]}]
            play = [
            {custom = [{smol = ["dd"]}]}
            ]
        "#;
        let parsed = parse(file_data).unwrap();
    }

        #[test]
    fn test_uneven() {
        let file_data = r#"
            [[define]]
            [[define.synth_defs]]
            brute = ["ccddggg"]
            smol = ["ee"]

            [[play]]
            times = 1

        "#;

        let parsed = parse(file_data).unwrap();
        assert_eq!(7, parsed.get("brute").unwrap().notes().len());
        assert_eq!(7, parsed.get("smol").unwrap().notes().len());
        let expected_padding = parsed.get("smol").unwrap().notes().last().unwrap();
        let expected_note = parsed.get("smol").unwrap().notes().get(5).unwrap();
        assert_eq!(&0.0, expected_padding.amplitude());
        assert_ne!(&0.0, expected_note.amplitude());
    }

}
