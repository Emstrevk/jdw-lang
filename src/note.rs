extern crate serde_json;

use serde::{Deserialize, Serialize};
use crate::tone::*;

/**
 * WARNING: Negative values can happen and are unsafe
 */
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct Note {
    tone: usize, // TODO: OCTAVE-bound struct
    sustain_time: f32,
    reserved_time: f32,
    amplitude: f32, // TODO: 1.0max struct
}

impl Note {
    /**
     * Lighter constructor for notes that
     * won't sound (amp 0). This should ideally
     * be another object implementing a common
     * trait, but for now let's keep it as a dead note.
     */
    pub fn quiet(p_dur: f32) -> Self {
        Note {
            tone: *C1.tone_index(),
            sustain_time: p_dur,
            reserved_time: p_dur,
            amplitude: 0.0,
        }
    }

    // Default constructor
    pub fn new(p_tone: Tone, p_sus: f32, p_dur: f32, p_amp: f32) -> Self {
        Note {
            tone: *p_tone.tone_index(),
            sustain_time: p_sus,
            reserved_time: p_dur,
            amplitude: p_amp,
        }
    }

    pub fn on(&self, p_tone: Tone) -> Self {
        let mut new_note = self.clone();
        new_note.tone = *p_tone.tone_index();
        new_note
    }

    pub fn amp(&self, new_amp: f32) -> Self {
        let mut new_note = self.clone();
        new_note.amplitude = new_amp;
        new_note
    }

    // Mutable getters for all attributes

    pub fn shift_tone(&mut self, by_index: i32) {
        let current_tone = self.tone_mut();
        let current_index = current_tone;
        self.tone = *Tone::new(current_index + by_index as usize).tone_index();
    }

    pub fn shift_amp(&mut self, by_amount: f32) {
        self.amplitude += by_amount;
    }

    /**
     * Adjust sustain to some percentage of reserved time
     */
    pub fn wrap_sustain(&mut self) {
        self.sustain_time = self.reserved_time * 0.9;
    }

    pub fn split(&mut self, divide_by: f32) {
        self.reserved_time = self.reserved_time / divide_by;
    }

    pub fn tone_mut(&mut self) -> usize {
        self.tone
    }
    pub fn sustain_time_mut(&mut self) -> &mut f32 {
        &mut self.sustain_time
    }
    pub fn reserved_time_mut(&mut self) -> &mut f32 {
        &mut self.reserved_time
    }
    pub fn amplitude_mut(&mut self) -> &mut f32 {
        &mut self.amplitude
    }

    // Non-mutable getters for all attributes
    pub fn tone(&self) -> &usize {
        &self.tone
    }
    pub fn sustain_time(&self) -> &f32 {
        &self.sustain_time
    }
    pub fn reserved_time(&self) -> &f32 {
        &self.reserved_time
    }
    pub fn amplitude(&self) -> &f32 {
        &self.amplitude
    }

    // Serialization work
    // Warning: HIghly unsafe
    pub fn to_json(&self) -> String {
        match serde_json::to_string(&self) {
            Ok(t) => t,
            Err(_e) => String::from("SERDE FAILURE; Check code"),
        }
    }

    pub fn to_code(&self) -> String {
        return format!(
            "Note({},{},{},{})",
            &self.tone, &self.sustain_time, &self.reserved_time, &self.amplitude
        );
    }
}
