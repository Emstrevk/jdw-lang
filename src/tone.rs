
use std::fmt;
use serde::{Deserialize, Serialize};

pub const C1: Tone = Tone::new(36);
pub const CS1: Tone = Tone::new(37);
pub const D1: Tone = Tone::new(38);
pub const DS1: Tone = Tone::new(39);
pub const E1: Tone = Tone::new(40);
pub const F1: Tone = Tone::new(41);
pub const FS1: Tone = Tone::new(42);
pub const G1: Tone = Tone::new(43);
pub const GS1: Tone = Tone::new(44);
pub const A1: Tone = Tone::new(45);
pub const AS1: Tone = Tone::new(46);
pub const B1: Tone = Tone::new(47);

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct Tone {
    tone_index: usize
}

impl Tone {

    pub const fn new(index: usize) -> Self {
        Tone {
            tone_index: index
        }
    }

    pub fn tone_index(&self) -> &usize {
        &self.tone_index
    }
}

// Basic display impl to support to_string()
// Note that I would prefer to print e.g. "C1" but string constants are tricky
impl fmt::Display for Tone {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Tone::new({})", self.tone_index)
    }
}
