/**
 * Score library, initial sketching
 */

use serde::{Deserialize, Serialize};

use crate::note::Note;
use log::{info, warn};

/**
 * Management struct for an underlying vector of
 *  Note objects.
 */
#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Score {
    notes: Vec<Note>
}

impl Score {

    /**
     * Initialize a new Score struct with an empty underlying
     *  Note vector.
     */
    pub fn new() -> Self {
        Score {
            notes: Vec::new()
        }

    }

    pub fn notes(&self) -> &Vec<Note> {
        &self.notes
    }

    /**
     * Adds a clone of the referenced note
     * If you want to add a reference (potentially several times),
     *  look elsewhere
     */
    pub fn add(&mut self, note: &Note) -> &mut Self {
        self.notes.push(note.clone());
        self
    }

    /**
     * Insert a quiet note long enough to match the
     * given "wait" time. TODO: negative safe
     */
    pub fn wait(&mut self, wait_time: f32) -> &mut Self {
        self.notes.push(Note::quiet(wait_time));
        self
    }

    /**
     * Add clones of all notes in another Score
     *  to the end of this Score.
     */
    pub fn append_score(&mut self, other: &Score) -> &mut Self {

        if other.notes().len() < 1 {
            info!("called append(other) with an empty other; this does nothing.");
            return self
        }

        for note in other.notes() {
            self.add(note);
        }
        self
    }

    /**
     * Mostly a demonstration of how to pass a closure as arg
     * Pass a closure that will be applied to all contained notes.
     */
    pub fn process_notes<F>(&mut self, mut f: F) -> &mut Self
    where F: FnMut(&mut Note) {

        for note in &mut self.notes {
            f(note);
        }

        self
    }


    /*
        Return a vector of notes representing &self's notes played and then padded with silence
            until matching length of other's notes
    */
    pub fn add_and_pad(&self, other: &Score) -> Vec<Note> {
        let mut keeper = Score::new();
        keeper.append_score(self);
        let left_to_pad = other.duration() - keeper.duration();
        keeper.wait(left_to_pad);
        keeper.notes().clone()
    }

    /*
        Return a vector of notes representing &self's notes looped until matching length
            of other's notes
    */
    pub fn repeat_with(&self, other: &Score) -> Vec<Note> {

        // Corner case: Don't get stuck in infinite loop trying to pad with self
        if self.duration() == 0.0 {
            return self.add_and_pad(other);
        }

        let mut keeper = Score::new();
        keeper.append_score(self);

        // Ideally you don't want overshoot when numbers don't add up
        while keeper.duration() + self.duration() <= other.duration() {
            keeper.append_score(self);
        }

        // Pad the last in case of non-matching denominator
        if keeper.duration() < other.duration() {
            let left_to_pad = other.duration() - keeper.duration();
            keeper.wait(left_to_pad);
        }

        keeper.notes.clone()
    }

    /**
     *  Add all the notes in the given list to the end of the score
     */
    pub fn read(&mut self, note_vector: Vec<Note>) -> &mut Self {

        for note in note_vector {
            self.notes.push(note);
        }

        self
    }

    /**
     * The total reserved time for all notes in the score
     * (Including silences)
     */
    pub fn duration(&self) -> f32 {
        let mut total_duration = 0.0;
        for note in &self.notes { total_duration += note.reserved_time() };
        total_duration
    }

    /**
     * Serialization.
     */
    pub fn to_json(&self) -> String {

        match serde_json::to_string(&self) {
            Ok(t) => {
                t
            },
            Err(_e) => String::from("SERDE FAILURE; Check code")
        }
    }

}
