
extern crate cpython;

use cpython::{PyResult, Python, py_module_initializer, py_fn, ToPyObject, PyDict};
use crate::note::Note;

pub mod score;
pub mod note;
pub mod tone;
pub mod note_strings;
pub mod toml_compose;
pub mod tone_translation;

py_module_initializer!(jackdaw, |py, m| {
    m.add(py, "__doc__", "This module is implemented in Rust.")?;
    m.add(py, "parse", py_fn!(py, parse_string(val: &str)))?;
    Ok(())
});

/*
    Python library additions for the core parsing functionality.
    This allows python to call "jackdaw.parse()" and return a
    vector of transformed note objects.

    There's still some work to be done to properly ready for python release:

    https://codeburst.io/how-to-use-rust-to-extend-python-360174ee5819
 */
fn parse_string(_py: Python, val: &str) -> PyResult<Vec<Note>> {
    Ok(note_strings::parse_notes(val))
}

impl ToPyObject for Note {
    type ObjectType = PyDict;

    fn to_py_object(&self, py: Python) -> PyDict {
        let dict = PyDict::new(py);
        dict.set_item(py, "tone", self.tone());
        dict.set_item(py, "sustain_time", self.sustain_time());
        dict.set_item(py, "reserved_time", self.reserved_time());
        dict.set_item(py, "amplitude", self.amplitude());
        dict
    }
}
