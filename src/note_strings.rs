
use crate::note::Note;
use crate::tone::Tone;
use log::info;
use crate::tone_translation::*;

const ABSOLUTE_MUTE: char = 'M';
const MULTIPLY_SYMBOL: char = ':';
const SAME_AS_LAST: char = '@';

const OPEN_WRAP: char = '[';
const CLOSE_WRAP: char = ']';
const NUMERIC_CHARS: &str = "123456789";
const SHARP_TONE: char = '*';
const BOOST_AMP: char = '!';
const DIVIDE_DUR: char = '/';
const PROLONG_NOTE: char = '-';
const SILENCE: char = '.';
const BLEED_SUS: char = '~'; // Might be tricky with brackets
static SINGLE_OPERATORS: &'static [char] = &[
SHARP_TONE, BOOST_AMP, DIVIDE_DUR, PROLONG_NOTE, BLEED_SUS
];

const BLEED_DIVIDER: i32 = 4;
const OCTAVE: i32 = 12;
const BASE_AMP: f32 = 0.56;
const AMP_STEP: f32 = 0.1;
const AMP_STEPS: &'static [f32] = &[0.0, 0.08, 0.16, 0.32];
const SUS_MULTI_STEPS: &'static [f32] = &[0.9, 1.0, 1.1, 1.5, 2.0, 3.0, 4.0, 5.0, 10.0];
const BASE_DUR: f32 = 1.0;
const TONE_STARTS_AT: i32 = 11;

const OPEN_GROUP: char = '(';
const CLOSE_GROUP: char = ')';
const DOUBLE_SET: char = 'x';

#[derive(PartialEq, Debug)]
struct Section {
    contents: String,
    operators: String,
    source: String
}

impl Section {

    // Take "(ccc)", remove frame: "ccc"
    fn new(cnt: &str, op: &str) -> Self  {

        let mut contents = String::from("");
        if cnt.len() > 2 {
            contents = String::from(&cnt[1..cnt.len()-1])
        }

        /*
        info!("DEBUG: Section created: no[]:{}:op:{}:full:{}",
            contents,
            String::from(op),
            format!("{}{}", cnt, op)
        );
        */

        Section {
            contents: contents,
            operators: String::from(op),
            source: format!("{}{}", cnt, op)
        }
    }
}

// "c[cc]" -> [Note(c, 1), Note(c, 0.5), Note(c, 0.5)]
pub fn parse_notes(note_string: &str) -> Vec<Note> {

    // Mute before anything else
    if note_string.contains(ABSOLUTE_MUTE) {
        return Vec::new();
    }

    // Remove whitespace
    let mut source = String::from(note_string);
    source.retain(|c| !c.is_whitespace());

    // Handle raw multipliers; "!:3"
    let multipliers_unwrapped = handle_multiplied_symbols(&source);

    // Remove square brackets, apply implicit cut operators + any regular
    let square_processed = de_wrap(&multipliers_unwrapped, "");

    // Remove parenthesis and apply regular multi-operators
    let group_processed = de_group(&square_processed, "");

    // Split by notes and silences
    let note_sections = split_by_note(&group_processed);

    let mut last_note = 'c';
    let dito_removed_note_sections: Vec<String> = note_sections.iter()
        .map(|section| {
            if section.contains(SAME_AS_LAST) {
                let new = section.replace(SAME_AS_LAST, &last_note.to_string());
                new
            } else {
                last_note = section.chars().next().unwrap();
                section.to_string()
            }
        }).collect();

    // parse_note() on each, return
    dito_removed_note_sections.iter().map(|section| {parse_note(section)})
            .collect()

}

// "c c/:4" -> "c c////"
fn handle_multiplied_symbols(note_string: &str) -> String {

    let mut expecting_number = false;
    let mut previous_symbol = ' ';
    let mut processed = String::from(note_string);

    // Sections in format "c:3"
    let mut multiply_operations = Vec::new();

    for char in note_string.chars() {
        if !expecting_number {
            if char == MULTIPLY_SYMBOL {
                expecting_number = true;
            } else {
                previous_symbol = char;
            }

        } else {
            if !NUMERIC_CHARS.contains(char) {
                panic!(
                    "Cannot use multiply operator '{}' without numeric char following: {}",
                    MULTIPLY_SYMBOL,
                    note_string
                );
            }

            if previous_symbol == ' ' {
                panic!(
                    "Cannot use multiply operator '{}' with nothing to multiply: {}",
                    MULTIPLY_SYMBOL,
                    note_string
                );
            }

            multiply_operations.push(
                format!("{}{}{}", previous_symbol, MULTIPLY_SYMBOL, char)
            );

            previous_symbol = ' ';
            expecting_number = false;

        }
    }

    for operation in multiply_operations {

        //info!("DEBUG: operation: {}", operation);

        // Ensured present by previous panic commands
        let symbol = &operation.chars().nth(0).unwrap();
        let amount = (&operation.chars().nth(2).unwrap().to_string()).parse::<i32>().unwrap();

        let replacement: String = (0..amount).map(|_| { symbol }).collect::<String>();

        processed = processed.replace(&operation, &replacement);
    }

    processed

}


fn is_operator(test: char) -> bool {
    SINGLE_OPERATORS.contains(&test)
    || NUMERIC_CHARS.contains(test)
    || test == DOUBLE_SET
}

fn de_wrap(source: &str, operators: &str) -> String {

    // When determining how short each contained note
    // is to become, one must account for [note,note] being "worth"
    // only [note]
    let count_full_notes = |test: &str| -> i32 {
        let sub = get_bracketed(OPEN_WRAP, CLOSE_WRAP, test);
        let mut mocked = String::from(test);
        for s in sub {
            mocked = mocked.replace(&s.source, "c");
        }

        let mut total_amount = 0;
        for note_section in split_by_note(&mocked) {
            let mut expected_dur = 1;
            for c in note_section.chars() {
                if c == PROLONG_NOTE {
                    expected_dur = expected_dur * 2;
                }
            }
            total_amount += expected_dur
        }

        total_amount

    };


    let sub_sections = get_bracketed(OPEN_WRAP, CLOSE_WRAP, source);

    let mut processed = String::from(source);

    // Run recursively to eliminate all levels of bracket
    for sub_section in sub_sections {

        // Handle the built-in wrap functionality;
        // add cut operators to match the amount of notes in a bracket
        let operator_equivalence : String =
            (0..count_full_notes(&sub_section.contents))
                .map(|_|{"/"})
                .collect::<String>();

        let joined_operators = format!(
            "{}{}", sub_section.operators, operator_equivalence
        );

        processed = processed.replace(
            &sub_section.source,
            &de_wrap(
                &sub_section.contents, &joined_operators
            )
        );

        //info!("DEBUG: Processed changed: {}", processed);
    }

    let output = apply_to_all(&processed, &operators);

    //info!("DEBUG: de_wrap: {}:{} => {}", source, operators, output);

    output

}

fn de_group(source: &str, operators: &str) -> String {

    // "ccc(cc)c(cc(dd))" -> cc cc(dd)
    let sub_sections = get_bracketed(OPEN_GROUP, CLOSE_GROUP, source);

    let mut processed = String::from(source);

    // Run recursively to eliminate all levels of bracket
    for sub_section in sub_sections {

        processed = processed.replace(
            &sub_section.source,
            &de_group(
                &sub_section.contents, &sub_section.operators
            )
        );
    }

    let output = apply_to_all(&processed, &operators);

    //info!("DEBUG: de_group: {}:{} => {}", source, operators, output);

    output
}

// Returns a map<section, operators> equivalent object of the top level brackets
// e.g. (cccc(cc)2)!cc(cc)* => <cccc(cc2), !>, <cc, *>
fn get_bracketed(opener: char, closer: char, note_string: &str) -> Vec<Section> {

    // Make sure bracket contents are sound to prevent scrambled result
    validate_brackets(note_string, opener, closer);

    let mut ret_vec = Vec::new();
    let mut current_bracketed = String::from("");
    let mut current_operators = String::from("");
    let mut opened_brackets = 0;
    for char in note_string.chars() {

        if char == opener { opened_brackets += 1; }
        if char == closer { opened_brackets -= 1; }

        // [cc][cc]

        if opened_brackets > 0 {

            // Scenario: We just opened the first bracket after closing
            // the last and we haven't performed cleanup yet.
            if opened_brackets == 1 && char == opener {
                if current_bracketed.len() > 0 {
                    ret_vec.push(
                        Section::new(
                            &current_bracketed.clone(),
                            &current_operators.clone()
                        )
                    );

                    current_bracketed = String::from("");
                    current_operators = String::from("");
                }
            }

            // Scenario: a new section starts right after operator
            //  of last one, e.g. (cc)3(cc) without any notes in between
            if current_operators.len() > 0 {
                ret_vec.push(
                    Section::new(
                        &current_bracketed.clone(),
                        &current_operators.clone()
                    )
                );

                current_bracketed = String::from("");
                current_operators = String::from("");
            }

            // Keep building until closed
            current_bracketed = format!("{}{}", current_bracketed, char);
        // Before anything opens current will be empty and shouldn't be saved
        } else if current_bracketed.len() > 0 {
            // If outer levels are closed we're waiting for
            // a non-operator to signal the end of operator collection,
            // after which we can save and return to search
            if is_operator(char) {
                current_operators = format!("{}{}", current_operators, char);
            } else if char == closer {
                // Right after closing we will have a closing bracket
                // at hand (in the same loop) which needs to be added to body
                // This is a bit clumsy; ideally you'd add both open and close
                // to body right away when calling open/close
                current_bracketed = format!("{}{}", current_bracketed, char);
            } else {
                // If we have closed the brackets and receive a char that is
                // not an operator, we can assume the bracketed section is at an end
                ret_vec.push(
                    Section::new(
                        &current_bracketed.clone(),
                        &current_operators.clone()
                    )
                );
                current_bracketed = String::from("");
                current_operators = String::from("");
            }
        }
    }

    // Scenario: There is no new note after the last section
    if current_bracketed.len() > 0 {
        ret_vec.push(
            Section::new(
                &current_bracketed.clone(),
                &current_operators.clone()
            )
        );
    }

    ret_vec

}

// "c*!!-//" -> Note()
fn parse_note(note_string: &str) -> Note {

    let first = note_string.chars().next().unwrap();

    // Verify that first char is a note bit
    if !NOTE_STARTERS.contains(&first) {
        panic!("Invalid first char in note string: {}", first);
    }

    // Establish starters for tone, sus, dur and amp
    let mut amp = BASE_AMP;
    let mut tone = match as_tone(first.clone()) {
        Ok(ok) => ok,
        Err(err) => {
            if first == SILENCE {
                amp = 0.0;
                0
            } else {
                panic!(err);
            }
        }
    } as i32;

    let mut dur_cuts = 0;
    let mut dur_power = 1;
    let mut sus_boosts = 0;
    let mut amp_boosts = 0;
    let mut tone_raises = 0;

    if note_string.len() > 1 {
        for c in String::from(&note_string[1..]).chars() {
            match c {
                BOOST_AMP => amp_boosts += 1,
                BLEED_SUS => sus_boosts += 1,
                DIVIDE_DUR => dur_cuts += 1,
                PROLONG_NOTE => dur_power += 1,
                SHARP_TONE => tone_raises += 1,
                _ => {
                    if NUMERIC_CHARS.contains(c) {
                        let num = (c.to_string()).parse::<i32>().unwrap();
                        tone_raises += (num-1) * OCTAVE;
                    } else {
                        panic!("Unexpeted note operator: {}", c)
                    }
                }
            }
        }
    }

    // Don't boost silence
    if amp != 0.0 {
        // Boost amp in the given exponential steps until maxed
        amp = amp + (AMP_STEPS.get(amp_boosts).unwrap_or(AMP_STEPS.last().unwrap()));
    }
    tone += tone_raises;
    let mut dur = BASE_DUR * dur_power as f32;
    info!("DEBUG: Dur before cut: {}, cuts: {}", dur, dur_cuts);
    if dur_cuts > 1 {
        dur = dur / dur_cuts as f32;
    }
    info!("DEBUG: Dur after dur cuts: {}", dur);

    // Sus, like amp, follows a static pattern of increments
    // For sus, however, it is in multipliers based on common usage rather than exponential/linear
    let sus_multiplier = SUS_MULTI_STEPS.get(sus_boosts).unwrap_or(SUS_MULTI_STEPS.last().unwrap());

    // Boosting sus of silences is pointless and in some buggy cases even harmful
    // e.g. currently the sequencer setup will add extra time per loop with end note sus
    let sus;
    if amp != 0.0 {
        sus = sus_multiplier * dur;
    } else {
        sus = dur;
    }

    // Return a note at the end
    let final_tone = Tone::new(tone as usize);
    Note::new(final_tone, sus, dur, amp)
}

// ccc, !* -> c!*c!*c!*
fn apply_to_all(note_string: &str, operator_string: &str) -> String {

    let mut final_set = String::from("");
    let mut multiply_set = 0;

    for operator in operator_string.chars() {
        if operator == DOUBLE_SET {
            multiply_set += 1;
        }
    }

    for char in note_string.chars() {

        let mut expanded_char = format!("{}", char);

        if NOTE_STARTERS.contains(&char){
            for operator in operator_string.chars() {
                if operator != DOUBLE_SET {
                    expanded_char = format!("{}{}", expanded_char, operator);
                }
            }
        }

        final_set = format!("{}{}", final_set, expanded_char);

    }

    let pre_multi_reference = final_set.clone();
    for _ in 0..multiply_set {
        final_set = format!("{}{}", final_set, pre_multi_reference);
        //info!("DEBUG: final set: {}", final_set);
    }

    final_set
}


fn validate_brackets(test: &str, opener: char, closer: char) {

    let mut open = 0;
    let mut closed = 0;

    for char in test.chars() {
        if char == opener {
            open += 1;
        } else if char == closer {
            closed +=1;
        }

        if closed > open {
            panic!("Illegal format: string {} has orphaned closing brackets", test);
        }
    }

    if open != closed {
        panic!("Illegal format: string {} does not close all brackets", test);
    }

}

// Turns a string into a list of substrings all starting with note_startes
// An applied "str.split()", in a sense
fn split_by_note(note_string: &str) -> Vec<String> {
    let mut note_section = String::from("");
    let mut note_sections = Vec::new();
    let mut last_note_char = 'c'; // Sensible default not to break on outliers
    for c in note_string.chars(){
        if NOTE_STARTERS.contains(&c) {
            if note_section != "" {
                note_sections.push(
                    note_section.clone()
                );
            }
            note_section = format!("{}", c);
        } else {
            note_section = format!("{}{}", note_section, c);
        }
    }
    if note_section != "" {
        note_sections.push(
            note_section.clone()
        );
    }

    note_sections
}

mod test {

    use crate::note_strings::*;

    #[test]
    fn test_de_wrap() {
        let full = "cc[dd]3c";
        let expected_parsed = "ccd3//d3//c";
        let parsed = de_wrap(full, "");
        assert_eq!(expected_parsed, parsed);

        let nested = "cc[dd[ff]d]d";
        let n_parse = "ccd////d////f//////f//////d////d";
        assert_eq!(n_parse, de_wrap(nested, ""));

        let cmb = "(cc[dd])3";
        assert_eq!("(ccd//d//)3", de_wrap(cmb, ""));

        // Mixed test territory
        assert_eq!(
                "ccd!//d!!//",
                de_group(&de_wrap("cc[d(d]!)!", ""), "")
        );

        assert_eq!(
            "c//c//c//c//",
            de_wrap("[cc][cc]", "")
        );

        assert_eq!(
            "(d////d////e////d////...)4",
            de_wrap("([dded]...)4", "")
        );

        assert_eq!(
            "c////.////.//",
            de_wrap("[[c.].]", "")
        );

        assert_eq!(
            "c////////--c////////--",
            de_wrap("[c--c--]", "")
        );

    }

    #[test]
    fn test_de_group() {
        let full = "ccc(dd)*cc(eee(ff)3)!!";
        let expected_parsed = "cccd*d*cce!!e!!e!!f!!3f!!3";

        let parsed = de_group(full, "");

        assert_eq!(expected_parsed, parsed);

        let multi = "(cc)x";
        assert_eq!("cccc", de_group(multi, ""));

    }

    #[test]
    fn test_get_bracketed() {
        let full = "cc(ddd)3*cc(dd(fff)!2)/1(cc)";
        let vec = get_bracketed(OPEN_GROUP, CLOSE_GROUP, full);

        let expected_vec = vec![
            Section::new("(ddd)", "3*"),
            Section::new("(dd(fff)!2)", "/1"),
            Section::new("(cc)", "")
        ];

        //info!("DEBUG: test_get_bracketed(): {:?}", vec);

        for ex in expected_vec {
            assert_eq!(
                true,
                vec.contains(&ex)
            );
        }

    }

    #[test]
    fn test_parse_note() {
        // G#, amp+secondstep, duration extended, sustain bled, cut
        let note = parse_note("g*!!-~//");
        assert_eq!(note.reserved_time(), &(BASE_DUR as f32));
        assert_eq!(note.amplitude(), &(BASE_AMP + (AMP_STEPS.get(2).unwrap())));

        let bleed_note = parse_note("f*~~~");
        let formula = SUS_MULTI_STEPS.get(3).unwrap() * BASE_DUR;
        assert_eq!(bleed_note.sustain_time(), &formula);
    }

    #[test]
    fn test_full_parse() {
        let parsed = parse_notes("c1* c3 - [f2 g* . .] h4");
        assert_eq!(7, parsed.len());
        let fourth = parsed.get(3).unwrap();
        assert_eq!(&(BASE_DUR/4 as f32), fourth.reserved_time());

        let corner_case = "[c-c.]";
        let notes = parse_notes(&corner_case);
        assert_eq!(&0.5, notes.get(0).unwrap().reserved_time());

    }

    #[test]
    fn test_apply_to_all() {
        let source = "c(c)c[cc]ff";
        let processed = apply_to_all(source, "!3");
        assert_eq!("c!3(c!3)c!3[c!3c!3]f!3f!3", processed);
    }

    #[test]
    fn test_validate_brackets_failure() {

        let assert_panic = |test: &str| {
            let result = std::panic::catch_unwind(|| validate_brackets(test, '[', ']'));
            assert!(result.is_err());
        };

        assert_panic("cc[cc");
        assert_panic("[](ff)]");
        assert_panic("fff)ff]fff[(");

    }

    #[test]
    fn test_multiply_symbol() {

        assert_eq!("cc////", &handle_multiplied_symbols("cc/:4"));
        assert_eq!("!!!!!!", &handle_multiplied_symbols("!:6"));


    }

}
