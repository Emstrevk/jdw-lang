
// Twelve notes for the twelve half-steps in an octave
pub static ASCII_LOWER: [char; 12] = [
    'a', 'b', 'c', 'd', 'e',
    'f', 'g', 'h', 'i', 'j',
    'k', 'l'
];

// Should really be the above PLUS the char constants but I dunno how to combine
//  static arrays...
pub static NOTE_STARTERS: [char; 14] = [
    'a', 'b', 'c', 'd', 'e',
    'f', 'g', 'h', 'i', 'j',
    'k', 'l', DITTO, SILENCE
];

pub static DITTO: char = '@';
pub static SILENCE: char = '.';

pub fn as_tone(letter: char) -> Result<usize, String> {
    ASCII_LOWER.iter().position(|l| l == &letter).ok_or("Not a tone char.".to_string())
}

mod test {

    use crate::tone_translation::*;

    #[test]
    fn test_tones() {
        assert_eq!(1, as_tone('b').unwrap());
        assert_eq!(3, as_tone('d').unwrap());
        assert_eq!(11, as_tone('l').unwrap());

        assert_eq!(Err("Not a tone char.".to_string()), as_tone(SILENCE));
        assert_eq!(Err("Not a tone char.".to_string()), as_tone('z'));
    }
}