# Jackdaw Composition Language
WIP library for code-driven music composition.

"Compiles" text lines such as "cc[cc2]daf.f" into vectors of note structs. 

The core parsing functionality is also available from the compiled python
    lib "jackdaw", see lib.rs.
    
```python
    import jackdaw

    notes = jackdaw.parse("c.[cd2]f")

    print(notes[0])
```

Currently in heavy flux as different design approaches are attempted. 

Ported from: https://bitbucket.org/Emstrevk/rust-compose/src/master/ 
